# React-compose
## Introduction


This is a collection of Higher Order Function that helps to compose React components


## Concept

  ### modifiers
Modifiers are props that will be converted to a class
  ### variables
Variables are props that will be converted to styles

## API
	
* withModifiers(namer[Function], modifiers[Array])
	Generate a new component that will accept Props presents in the modifiers Array. For each passed prop it will call the namer function 
* withVariables  = (keyEnhancer[Function], valEnhancer[Function], variables[Array])

* keyEnhancer =  prop, value, all props => new key
* valueEnhancer =  value => value

## Re-usable & composable Properties

For example, you want to create components That support getting a grid 
area.



	import {withVariables,withBaseClass} from  '@karsegard/react-compose'
	
	export  const  withGridArea  =  withVariables(
				_  =>  `gridArea`,
				x  =>  `${x}`,
				['area']
	);

  
  	export  const  ComponentWithArea  =  compose(
		withGridArea,
		withBaseClass('area-component')
	)(divElement);

	// somewhere Else
	...
	let MyComponentWithArea = withArea(MyExistingComponent)
	return (<MyComponentWithArea area="header"/>)




## Install

  

```bash

yarn add @karsegard/react-compose

```

  

## Usage

see [example](example)
 

## License

  

MIT © [FDT2k](https://github.com/FDT2k)
