'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var React = require('react');
var ReactUtils = require('@karsegard/composite-js/ReactUtils');
var compositeJs = require('@karsegard/composite-js');
require('@karsegard/composite-js/ObjectUtils');
var cex = require('@karsegard/cex');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var React__default = /*#__PURE__*/_interopDefaultLegacy(React);

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);

    if (enumerableOnly) {
      symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
    }

    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread2(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _typeof(obj) {
  "@babel/helpers - typeof";

  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function (obj) {
      return typeof obj;
    };
  } else {
    _typeof = function (obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};

  var target = _objectWithoutPropertiesLoose(source, excluded);

  var key, i;

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) return _arrayLikeToArray(arr);
}

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

function _iterableToArray(iter) {
  if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
}

function _iterableToArrayLimit(arr, i) {
  var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"];

  if (_i == null) return;
  var _arr = [];
  var _n = true;
  var _d = false;

  var _s, _e;

  try {
    for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

  return arr2;
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

var _excluded = ["children"],
    _excluded2 = ["children"],
    _excluded3 = ["children"],
    _excluded4 = ["children"],
    _excluded5 = ["children"],
    _excluded6 = ["className"],
    _excluded7 = ["className"],
    _excluded8 = ["className"],
    _excluded9 = ["BEM", "className"],
    _excluded10 = ["className"],
    _excluded11 = ["style"],
    _excluded12 = ["className"];
var e = React__default['default'].createElement;

var bem = function bem(main) {
  return [main, function (block) {
    return "".concat(main, "__").concat(block);
  }, function (modifier) {
    return "".concat(main, "--").concat(modifier);
  }];
};

var makeBEM = function makeBEM(current) {
  return {
    current: current,
    make: {
      block: function block(_block) {
        return makeBEM("".concat(current, "-").concat(_block));
      },
      element: function element(_element) {
        return makeBEM("".concat(current, "__").concat(_element));
      },
      modifier: function modifier(_modifier) {
        return makeBEM("".concat(current, "--").concat(_modifier));
      }
    },
    block: function block(_block2) {
      return "".concat(current, "-").concat(_block2);
    },
    element: function element(_element2) {
      return "".concat(current, "__").concat(_element2);
    },
    modifier: function modifier(_modifier2) {
      return "".concat(current, "--").concat(_modifier2);
    }
  };
};
var wrapComponent = function wrapComponent(Wrap) {
  return function (Component) {
    return function (_ref) {
      var children = _ref.children,
          rest = _objectWithoutProperties(_ref, _excluded);

      return /*#__PURE__*/React__default['default'].createElement(Wrap, rest, /*#__PURE__*/React__default['default'].createElement(Component, null, children));
    };
  };
};
var divElement = function divElement(_ref2) {
  var children = _ref2.children,
      rest = _objectWithoutProperties(_ref2, _excluded2);

  return /*#__PURE__*/React__default['default'].createElement("div", rest, children);
};
var sectionElement = function sectionElement(_ref3) {
  var children = _ref3.children,
      rest = _objectWithoutProperties(_ref3, _excluded3);

  return /*#__PURE__*/React__default['default'].createElement("section", rest, children);
};
var asideElement = function asideElement(_ref4) {
  var children = _ref4.children,
      rest = _objectWithoutProperties(_ref4, _excluded4);

  return /*#__PURE__*/React__default['default'].createElement("aside", rest, children);
};
var baseElement = compositeJs.curry(function (_e, _ref5) {
  var children = _ref5.children,
      rest = _objectWithoutProperties(_ref5, _excluded5);

  return e(_e, rest, children);
});
var modifiersToCeX = function modifiersToCeX(keyEnhancer, list, modifiers) {
  var props = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  return list.reduce(function (acc, item) {
    var _type = _typeof(modifiers[item]);

    acc[keyEnhancer(item, modifiers[item], props)] = function (_) {
      return _type !== 'undefined' && modifiers[item] !== false;
    };

    return acc;
  }, {});
};
var withBaseClass = function withBaseClass(BaseClass) {
  return function (Component) {
    return function (props) {
      var className = props.className,
          rest = _objectWithoutProperties(props, _excluded6);

      var classes = cex.cEx([BaseClass, className]);
      return /*#__PURE__*/React__default['default'].createElement(Component, _extends({}, rest, {
        className: classes
      }));
    };
  };
};
var getClasseNames = function getClasseNames(BaseClass, props) {
  var className = props.className,
      rest = _objectWithoutProperties(props, _excluded7);

  var classes = cex.cEx([BaseClass, className]);
  return _objectSpread2({
    className: classes
  }, rest);
};
var withBEM = function withBEM(BEM) {
  return function (Component) {
    return function (props) {
      var className = props.className,
          rest = _objectWithoutProperties(props, _excluded8);

      var classes = cex.cEx([BEM.current, className]);
      rest.BEM = BEM.make;
      return /*#__PURE__*/React__default['default'].createElement(Component, _extends({}, rest, {
        className: classes
      }));
    };
  };
};
var withBEMElement = function withBEMElement(element) {
  return function (Component) {
    return function (props) {
      var BEM = props.BEM,
          className = props.className,
          rest = _objectWithoutProperties(props, _excluded9);

      if (compositeJs.is_nil(BEM)) {
        console.warn('withBEMElement used without parent BEM');
      }

      var _BEM = BEM.element('item');

      var classes = cex.cEx([_BEM.current, className]);
      return /*#__PURE__*/React__default['default'].createElement(Component, _extends({}, rest, {
        className: classes
      }));
    };
  };
};
var withModifiers = function withModifiers(namer, modifiers) {
  return function (Component) {
    return function (props) {
      var className = props.className,
          rest = _objectWithoutProperties(props, _excluded10); //ensure to preserve classNames


      var _spreadObjectPresentI = ReactUtils.spreadObjectPresentIn(modifiers, rest),
          _spreadObjectPresentI2 = _slicedToArray(_spreadObjectPresentI, 2),
          presentModifiers = _spreadObjectPresentI2[0],
          _props = _spreadObjectPresentI2[1];

      var classes = cex.cEx([className, modifiersToCeX(namer, modifiers, presentModifiers, props)]);
      return /*#__PURE__*/React__default['default'].createElement(Component, _extends({
        className: classes
      }, _props));
    };
  };
};
/*
apply a modifier class if modifiers's key is true
*/

var withBEMModifiers = function withBEMModifiers(modifiers) {
  return withModifiers(function (modifier, _, _ref6) {
    var BEM = _ref6.BEM;
    return BEM.modifier(modifier).current;
  }, modifiers);
};
var reduceVariables = function reduceVariables(keyEnhancer, valEnhancer, list, variables) {
  return list.reduce(function (acc, item) {
    if (typeof variables[item] !== 'undefined') {
      acc[keyEnhancer(item, variables[item])] = valEnhancer(variables[item]);
    }

    return acc;
  }, {});
};
var withVariables = function withVariables(keyEnhancer, valEnhancer, variables) {
  return function (Component) {
    return function (props) {
      var style = props.style,
          rest = _objectWithoutProperties(props, _excluded11); //ensure to preserve styles


      var _style = style || {};

      var _spreadObjectPresentI3 = ReactUtils.spreadObjectPresentIn(variables, rest),
          _spreadObjectPresentI4 = _slicedToArray(_spreadObjectPresentI3, 2),
          presentVars = _spreadObjectPresentI4[0],
          _props = _spreadObjectPresentI4[1];

      var styles = _objectSpread2(_objectSpread2({}, _style), reduceVariables(keyEnhancer, valEnhancer, variables, presentVars));

      return /*#__PURE__*/React__default['default'].createElement(Component, _extends({
        style: styles
      }, _props));
    };
  };
};
var propsToCeX = function propsToCeX(keyEnhancer, list, modifiers) {
  return list.reduce(function (acc, item) {
    if (modifiers[item]) {
      acc.push(function (_) {
        return keyEnhancer(modifiers[item]);
      });
    }

    return acc;
  }, []);
};
var withTransformedProps = function withTransformedProps(namer, modifiers) {
  return function (Component) {
    return function (props) {
      var className = props.className,
          rest = _objectWithoutProperties(props, _excluded12); //ensure to preserve classNames


      var _spreadObjectPresentI5 = ReactUtils.spreadObjectPresentIn(modifiers, rest),
          _spreadObjectPresentI6 = _slicedToArray(_spreadObjectPresentI5, 2),
          presentModifiers = _spreadObjectPresentI6[0],
          _props = _spreadObjectPresentI6[1]; // console.error( enlist(presentModifiers),modifiers)
      //console.log(propsToCeX(namer,modifiers, presentModifiers))


      var classes = cex.cEx([className].concat(_toConsumableArray(propsToCeX(namer, modifiers, presentModifiers))));
      return /*#__PURE__*/React__default['default'].createElement(Component, _extends({
        className: classes
      }, _props));
    };
  };
}; // apply modifiers if none of unless is present in props

var applyModifiers = function applyModifiers(modifiers, unless) {
  return function (Component) {
    return function (props) {
      var _m;

      if (unless && unless.length > 0) {
        var found = false;

        for (var _i = 0, _Object$keys = Object.keys(props); _i < _Object$keys.length; _i++) {
          var prop = _Object$keys[_i];

          if (unless.indexOf(prop) !== -1) {
            found = true;
          }
        }

        if (!found) {
          _m = modifiers;
        }
      } else {
        _m = modifiers;
      }

      return /*#__PURE__*/React__default['default'].createElement(Component, _extends({}, _m, props));
    };
  };
};

var makePropsFilter = function makePropsFilter(prefix) {
  return [ReactUtils.spreadObjectBeginWith(prefix), ReactUtils.forwardPropsRemovingHeader(prefix)];
};

var kebabize = function kebabize(str) {
  return str.replace(/[A-Z]+(?![a-z])|[A-Z]/g, function ($, ofs) {
    return (ofs ? "-" : "") + $.toLowerCase();
  });
};
var snakize = function snakize(str) {
  return str.replace(/[A-Z]+(?![a-z])|[A-Z]/g, function ($, ofs) {
    return (ofs ? "_" : "") + $.toLowerCase();
  });
};
var camelize = function camelize(text) {
  return text.replace(/^([A-Z])|[\s-_]+(\w)/g, function (_, p1, p2, __) {
    return p2 ? p2.toUpperCase() : p1.toLowerCase();
  });
};

Object.defineProperty(exports, 'filterPropPresentIn', {
enumerable: true,
get: function () {
return ReactUtils.spreadObjectPresentIn;
}
});
Object.defineProperty(exports, 'filterPropStartingWith', {
enumerable: true,
get: function () {
return ReactUtils.spreadObjectBeginWith;
}
});
Object.defineProperty(exports, 'forwardProps', {
enumerable: true,
get: function () {
return ReactUtils.forwardPropsRemovingHeader;
}
});
Object.defineProperty(exports, 'compose', {
enumerable: true,
get: function () {
return compositeJs.compose;
}
});
Object.defineProperty(exports, 'cEx', {
enumerable: true,
get: function () {
return cex.cEx;
}
});
Object.defineProperty(exports, 'classNames', {
enumerable: true,
get: function () {
return cex.cEx;
}
});
exports.applyModifiers = applyModifiers;
exports.asideElement = asideElement;
exports.baseElement = baseElement;
exports.bem = bem;
exports.camelize = camelize;
exports.divElement = divElement;
exports.e = e;
exports.getClasseNames = getClasseNames;
exports.kebabize = kebabize;
exports.makeBEM = makeBEM;
exports.makePropsFilter = makePropsFilter;
exports.modifiersToCeX = modifiersToCeX;
exports.propsToCeX = propsToCeX;
exports.reduceVariables = reduceVariables;
exports.sectionElement = sectionElement;
exports.snakize = snakize;
exports.withBEM = withBEM;
exports.withBEMElement = withBEMElement;
exports.withBEMModifiers = withBEMModifiers;
exports.withBaseClass = withBaseClass;
exports.withModifiers = withModifiers;
exports.withTransformedProps = withTransformedProps;
exports.withVariables = withVariables;
exports.wrapComponent = wrapComponent;
