import React from 'react'


import { withModifiers, divElement, compose, withBaseClass } from '@karsegard/react-compose'

import './index.css'


const Component = divElement;


const Nice = withModifiers(x => x, ['nice'])
const Wonderful = withModifiers(x => `very_${x}`, ['wonderful'])
const NiceComponent = Nice(Component);
const WonderfulComponent = Wonderful(Component);
const NiceAndWonderFulComponent = compose(Nice, Wonderful)(Component);


const Flexify = compose(

                  withModifiers(x=>x, ['center','start','end','between','around']),
                  withBaseClass('flexified')
                ) 


const Flex = Flexify(Component)


const App = () => {

  return (
    <div>
      <h1>Quick Start</h1>
      <Component nice wonderful
      >This is the base component
      </Component>
      <NiceComponent nice wonderful>
        Nice component now accepts a nice prop which is applied as a class. Remember "modifiers" are class
      </NiceComponent>

      <WonderfulComponent nice wonderful>
        Wonderful component accepts wonderful but not nice. Wonderful is transformed to very_wonderful
      </WonderfulComponent>


      <NiceAndWonderFulComponent nice wonderful>
        This accepts both modifiers
      </NiceAndWonderFulComponent>

      
      <h1>Let's make a front-end framework</h1>

      <Component>at first there was nothing</Component>
      
      <Flex around>
        <Component>Then we wanted flex</Component>
        <Component>That's right</Component>
      </Flex>




    </div>
  )
}
export default App
